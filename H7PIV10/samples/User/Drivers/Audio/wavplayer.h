#ifndef __PLAYER_WAV_H__
#define __PLAYER_WAV_H__


#ifdef __cplusplus
extern "C" {
#endif
#include "sys.h"

typedef enum
{
  LittleEndian = 0,
  BigEndian
}Endianness_t;


/*********************************************
 * 
 * wavplayer device
 * 
 * ******************************************/
typedef struct
{
	player_dev_t*     player;
	recorder_dev_t*   recorder;
	
}wavplayer_dev_t;

/*********************************************
 * 
 * data struct
 * 
 * ******************************************/
typedef struct
{
  uint16_t  FormatTag;
  uint16_t  NumChannels;
  uint16_t  BlockAlign;
  uint16_t  BitsPerSample;
  uint32_t  RIFFchunksize;
  uint32_t  SampleRate;
  uint32_t  ByteRate;
  uint32_t  DataSize;
}wav_format_t;


/* Exported constants --------------------------------------------------------*/
#define  CHUNK_ID                            0x52494646  /* correspond to the letters 'RIFF' */
#define  FILE_FORMAT                         0x57415645  /* correspond to the letters 'WAVE' */
#define  FORMAT_ID                           0x666D7420  /* correspond to the letters 'fmt ' */
#define  DATA_ID                             0x64617461  /* correspond to the letters 'data' */
#define  FACT_ID                             0x66616374  /* correspond to the letters 'fact' */
#define  WAVE_FORMAT_PCM                     0x01
#define  FORMAT_CHNUK_SIZE                   0x10
#define  CHANNEL_MONO                        0x01
#define  CHANNEL_STEREO                      0x02
#define  SAMPLE_RATE_8000                    8000
#define  SAMPLE_RATE_11025                   11025
#define  SAMPLE_RATE_22050                   22050
#define  SAMPLE_RATE_44100                   44100
#define  BITS_PER_SAMPLE_8                   8
#define  BITS_PER_SAMPLE_16                  16

/******************************************************
* 
* wavplayer api
* 
* ****************************************************/
void     wavplayer_link(player_dev_t* player, recorder_dev_t* recorder);
void     wavplayer_unlink(void);
/******************************************************
* 
* player api
* 
* ****************************************************/
uint8_t  wavplayer_parse(wav_format_t* fmt, uint8_t* data, uint32_t* offset);
void     wavplayer_shutdown(uint8_t sd);
uint8_t  wavplayer_player_start(char* path);
uint8_t  wavplayer_player_stop(void);
/******************************************************
* 
* recorder api
* 
* ****************************************************/

uint8_t  wavplayer_recorder_start(char* path);
uint8_t  wavplayer_recorder_stop(void);


#ifdef __cplusplus
}
#endif

#endif

