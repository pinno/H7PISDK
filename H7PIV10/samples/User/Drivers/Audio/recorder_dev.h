#ifndef __RECORDER_DEV_H__
#define __RECORDER_DEV_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "stdint.h"


#define RECORDER_BUFFER_SIZE 128
/*****************************************************
 * 
 * RECORDER status definition
 * 
 ****************************************************/
typedef enum{
    RECORDER_STOP=0,
    RECORDER_START,
    RECORDER_PAUSE,
    RECORDER_RECORDING,
    RECORDER_INVALID,
    RECORDER_STATUS_NUM
}recorder_dev_status_t;
/*****************************************************
 * 
 * recorder dev definition
 * 
 ****************************************************/
typedef struct 
{
    uint32_t buffer_size;
    uint32_t *(*length_array)(uint8_t buffer);

    void     (*deinit)(void);

    uint8_t  (*stop)(void);
    uint8_t  (*start)(void);

    uint8_t  (*set_samplerate)(uint32_t samplerate);
    uint8_t  (*set_buffer_valid)(uint16_t* buffer, uint8_t state);
    uint16_t  *(*get_buffer_pos)(void);
    recorder_dev_status_t (*get_status)(void);

    void     (*delay_ms)(uint32_t ms);
    void     (*delay_us)(uint32_t us);
		
}recorder_dev_t;


#ifdef __cplusplus
}
#endif


#endif

