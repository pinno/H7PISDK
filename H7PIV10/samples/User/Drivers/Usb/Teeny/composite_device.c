/*       
 *         _______                    _    _  _____ ____  
 *        |__   __|                  | |  | |/ ____|  _ \ 
 *           | | ___  ___ _ __  _   _| |  | | (___ | |_) |
 *           | |/ _ \/ _ \ '_ \| | | | |  | |\___ \|  _ < 
 *           | |  __/  __/ | | | |_| | |__| |____) | |_) |
 *           |_|\___|\___|_| |_|\__, |\____/|_____/|____/ 
 *                               __/ |                    
 *                              |___/                     
 *
 * TeenyUSB - light weight usb stack for STM32 micro controllers
 * 
 * Copyright (c) 2019 XToolBox  - admin@xtoolbox.org
 *                         www.tusb.org
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
 
/****************************
user's drivers
*****************************/
#include "spiflash.h"

#include "teeny_usb.h"
#include "tusbd_user.h"
#include "tusbd_hid.h"
#include "tusbd_cdc.h"
#include "tusbd_msc.h"

#define USER_RX_EP_SIZE   32
#define CDC_RX_EP_SIZE    32
#define HID_RX_EP_SIZE    16
#define HID_REPORT_DESC         COMP_ReportDescriptor_if0
#define HID_REPORT_DESC_SIZE    COMP_REPORT_DESCRIPTOR_SIZE_IF0

/*********************************************************************************
allocate more buffer for better performance

*********************************************************************************/
__ALIGN_BEGIN uint8_t user_buf[USER_RX_EP_SIZE*4] __ALIGN_END;

int user_recv_data(tusb_user_device_t* raw, const void* data, uint16_t len);
int user_send_done(tusb_user_device_t* raw);

tusb_user_device_t user_dev = {
  .backend = &user_device_backend,
  .ep_in = 3,
  .ep_out = 3,
  .on_recv_data = user_recv_data,
  .on_send_done = user_send_done,
  .rx_buf = user_buf,
  .rx_size = sizeof(user_buf),
};

/*********************************************************************************
The HID recv buffer size must equal to the out report size

*********************************************************************************/
__ALIGN_BEGIN uint8_t hid_buf[HID_RX_EP_SIZE] __ALIGN_END;
int hid_recv_data(tusb_hid_device_t* hid, const void* data, uint16_t len);
int hid_send_done(tusb_hid_device_t* hid);

tusb_hid_device_t hid_dev = {
  .backend = &hid_device_backend,
  .ep_in = 2,
  .ep_out = 2,
  .on_recv_data = hid_recv_data,
  .on_send_done = hid_send_done,
  .rx_buf = hid_buf,
  .rx_size = sizeof(hid_buf),
  .report_desc = HID_REPORT_DESC,
  .report_desc_size = HID_REPORT_DESC_SIZE,
};

/*********************************************************************************
// The CDC recv buffer size should equal to the out endpoint size
// or we will need a timeout to flush the recv buffer
*********************************************************************************/
__ALIGN_BEGIN uint8_t cdc_buf[CDC_RX_EP_SIZE] __ALIGN_END;

int cdc_recv_data(tusb_cdc_device_t* cdc, const void* data, uint16_t len);
int cdc_send_done(tusb_cdc_device_t* cdc);
void cdc_line_coding_change(tusb_cdc_device_t* cdc);

tusb_cdc_device_t cdc_dev = {
  .backend = &cdc_device_backend,
  .ep_in = 1,
  .ep_out = 1,
  .ep_int = 8,
  .on_recv_data = cdc_recv_data,
  .on_send_done = cdc_send_done,
  .on_line_coding_change = cdc_line_coding_change,
  .rx_buf = cdc_buf,
  .rx_size = sizeof(cdc_buf),
};


int msc_get_cap(tusb_msc_device_t* msc, uint8_t lun, uint32_t *block_num, uint32_t *block_size);
int msc_block_read(tusb_msc_device_t* msc, uint8_t lun, uint8_t *buf, uint32_t block_addr, uint16_t block_len);
int msc_block_write(tusb_msc_device_t* msc, uint8_t lun, const uint8_t *buf, uint32_t block_addr, uint16_t block_len);

tusb_msc_device_t msc_dev = {
  .backend = &msc_device_backend,
  .ep_in = 4,
  .ep_out = 4,
  .max_lun = 0, // 1 logic unit
  .get_cap = msc_get_cap,
  .block_read = msc_block_read,
  .block_write = msc_block_write,
};

/*********************************************************************************
// make sure the interface order is same in "composite_desc.lua"

*********************************************************************************/
static tusb_device_interface_t* device_interfaces[] = {
  (tusb_device_interface_t*)&hid_dev,
  (tusb_device_interface_t*)&cdc_dev, 0,   // CDC need two interfaces
  (tusb_device_interface_t*)&user_dev,
  (tusb_device_interface_t*)&msc_dev,
};

static void init_ep(tusb_device_t* dev)
{
  COMP_TUSB_INIT(dev);
}

tusb_device_config_t device_config = {
  .if_count = sizeof(device_interfaces)/sizeof(device_interfaces[0]),
  .interfaces = &device_interfaces[0],
  .ep_init = init_ep,
};

void tusb_delay_ms(uint32_t ms)
{
  uint32_t i,j;
  for(i=0;i<ms;++i)
    for(j=0;j<200;++j);
}



static int user_len = 0;
int user_recv_data(tusb_user_device_t* raw, const void* data, uint16_t len)
{
  user_len = (int)len;
  return 1; // return 1 means the recv buffer is busy
}

int user_send_done(tusb_user_device_t* raw)
{
  tusb_set_rx_valid(raw->dev, raw->ep_out);
  return 0;
}

static int hid_len = 0;
int hid_recv_data(tusb_hid_device_t* hid, const void* data, uint16_t len)
{
  hid_len = (int)len;
  return 1; // return 1 means the recv buffer is busy
}

int hid_send_done(tusb_hid_device_t* hid)
{
  tusb_set_rx_valid(hid->dev, hid->ep_out);
  return 0;
}

static int cdc_len = 0;
int cdc_recv_data(tusb_cdc_device_t* cdc, const void* data, uint16_t len)
{
  cdc_len = (int)len;
  return 1; // return 1 means the recv buffer is busy
}

int cdc_send_done(tusb_cdc_device_t* cdc)
{
  tusb_set_rx_valid(cdc->dev, cdc->ep_out);
  return 0;
}

void cdc_line_coding_change(tusb_cdc_device_t* cdc)
{
  // TODO, handle the line coding change
  //cdc->line_coding.bitrate;
  //cdc->line_coding.databits;
  //cdc->line_coding.stopbits;
  //cdc->line_coding.parity;
}

#if (SPI_FLASH_TYPE ==  W25Q64)
#define BLOCK_SIZE   SPI_FLASH_SECTOR_SIZE
#define BLOCK_COUNT  SPI_FLASH_SECTOR_COUNT


int msc_get_cap(tusb_msc_device_t* msc, uint8_t lun, uint32_t *block_num, uint32_t *block_size) {
	HAL_SD_CardInfoTypeDef info;
	switch(lun)
	{
		case 0:
			*block_size=SPI_FLASH_SECTOR_SIZE;  
			*block_num=SPI_FLASH_SECTOR_COUNT; //8M spi flash
			break;
		case 1:
			break;
	}
  return 0;
}

int msc_block_read(tusb_msc_device_t* msc, uint8_t lun, uint8_t *buf, uint32_t block_addr, uint16_t block_len)
{	
	uint32_t len = 0;
	switch(lun)
	{
		case 0:
			len = block_len*BLOCK_SIZE;
			W25Q_SPI_FastRead(buf,block_addr*SPI_FLASH_SECTOR_SIZE,block_len*SPI_FLASH_SECTOR_SIZE);
			break;
		case 1://SD
			
			break; 
	} 
  return (block_len*SPI_FLASH_SECTOR_SIZE);
}

int msc_block_write(tusb_msc_device_t* msc, uint8_t lun, const uint8_t *buf, uint32_t block_addr, uint16_t block_len)
{
  uint32_t len = 0;
	switch(lun)
	{
		case 0://spi FLASH
			len = block_len*BLOCK_SIZE;
			W25Q_SPI_Write((uint8_t*)buf,block_addr*SPI_FLASH_SECTOR_SIZE,block_len*SPI_FLASH_SECTOR_SIZE);
			break;
		case 1://SD
			break; 
	}
  return len;
}

#else
#warning MSC storage not set
#endif




#if MSC_DATA_PACKET_LENGTH < BLOCK_SIZE
#error MSC data packet buffer is small than the block size
#endif

/**********************************************************

 a usb task init 

**********************************************************/
void MX_USB_DEVICE_Init(void)
{
  tusb_device_t* dev = tusb_get_device(TEST_APP_USB_CORE);
  tusb_set_device_config(dev, &device_config);
  tusb_open_device(dev);
}

/**********************************************************

 a usb task loop 

**********************************************************/
extern void cdc_receive(uint8_t* Buf, uint16_t Len);

int tusb_composite_task(void)
{
  while(1)
	{
    if(cdc_len){
      cdc_receive(cdc_buf,cdc_len);
			cdc_len = 0;
    }
    tusb_msc_device_loop(&msc_dev);
  }
}
